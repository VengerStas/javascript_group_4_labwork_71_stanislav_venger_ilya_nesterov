import React, { Component } from 'react';
import './App.css';
import {Route, Switch} from "react-router-dom";
import DishesList from "./containers/DishesList/DishesList";
import AddDishes from "./containers/AddDishes/AddDishes";
import EditDishes from "./containers/EditDishes/EditDishes";
import Orders from "./containers/Orders/Orders";

class App extends Component {
  render() {
    return (
      <Switch>
        <Route path='/' exact component={DishesList}/>
        <Route path='/orders' exact component={Orders}/>
        <Route path='/add-dish' exact component={AddDishes}/>
        <Route path='/:id/edit' exact component={EditDishes}/>
        <Route render={() => <h1>Page not found</h1>}/>
      </Switch>
    );
  }
}

export default App;
