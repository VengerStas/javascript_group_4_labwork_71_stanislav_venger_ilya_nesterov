import axios from "../../axiosAdmin";
import {dishesFailure, dishesRequest, fetchGetDishes} from "./cardBuilder";

export const fetchPostDish = (dish) => {
    return (dispatch) => {
        dispatch(dishesRequest());
        axios.post('dishes.json', dish).then(() => {
            dispatch(dishesRequest());
            dispatch(fetchGetDishes());
        }, error => {
            dispatch(dishesFailure(error));
        })
    }
};
