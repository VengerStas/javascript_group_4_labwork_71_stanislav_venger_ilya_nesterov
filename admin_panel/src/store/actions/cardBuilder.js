import axios from '../../axiosAdmin';
import {
    DISH_REQUEST_SUCCESS,
    DISHES_FAILURE,
    DISHES_REQUEST,
    DISHES_REQUEST_SUCCESS,
    ORDERS_REQUEST_SUCCESS
} from "./actionTypes";


export const dishesRequest = () => {
    return {type: DISHES_REQUEST};
};

export const dishesSuccess = dishes => {
    return {type: DISHES_REQUEST_SUCCESS, dishes};
};

export const ordersSuccess = orders => {
    return {type: ORDERS_REQUEST_SUCCESS, orders};
};

export const dishesFailure = error => {
    return {type: DISHES_FAILURE, error};
};

export const fetchGetDishes = () => {
    return (dispatch) => {
        dispatch(dishesRequest());
        axios.get('dishes.json').then(response => {
            dispatch(dishesSuccess(response.data));
        }, error => {
            dispatch(dishesFailure(error));
        });
    }
};

export const fetchGetOrders = () => {
    return (dispatch) => {
        dispatch(dishesRequest());
        axios.get('orders.json').then(response => {
            dispatch(ordersSuccess(response.data));
        }, error => {
            dispatch(dishesFailure(error));
        });
    }
};


export const deleteDish = (dishId) => {
    return (dispatch) => {
        dispatch(dishesRequest());
        axios.delete('/dishes/' + dishId + '.json').then(() => {
            dispatch(fetchGetDishes());
        })
    }
};

export const deleteOrder = (orderId) => {
    return (dispatch) => {
        dispatch(ordersSuccess());
        axios.delete('/orders/' + orderId + '.json').then(() => {
            dispatch(fetchGetOrders());
        })
    }
};

export const editDish = (dishId) => {
    return (dispatch) => {
        dispatch(dishesRequest());
        return axios.get('dishes/' + dishId + '.json').then((response) => {
            dispatch({type: DISH_REQUEST_SUCCESS, dish: response.data});
        })
    }
};

export const saveEditDish = (dishId, dish) => {
    return (dispatch) => {
        dispatch(dishesRequest());
        axios.put('dishes/' + dishId + '.json', dish).then(() => {
            dispatch(fetchGetDishes());
        })
    }
};
