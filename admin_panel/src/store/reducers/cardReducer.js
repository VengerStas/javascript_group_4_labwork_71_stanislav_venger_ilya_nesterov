import {
    DELETE_DISHES, DELETE_ORDERS,
    DISH_REQUEST_SUCCESS,
    DISHES_REQUEST_SUCCESS,
    ORDERS_REQUEST_SUCCESS
} from "../actions/actionTypes";

const initialState = {
    dishes: {},
    dish: null,
    orders: null,
};

const cardReducer = (state = initialState, action) => {
    switch (action.type) {
        case DISHES_REQUEST_SUCCESS:
            return {
                ...state,
                dishes: action.dishes,
            };

        case DISH_REQUEST_SUCCESS:
            return {
                ...state,
                dish: action.dish
            };
        case ORDERS_REQUEST_SUCCESS:
            return {
                ...state,
                dishes: action.dishes,
                orders: action.orders,
            };
        case DELETE_DISHES:
            return {
                ...state,
            };
        case DELETE_ORDERS:
            return {
                ...state,
            };
        default:
            return state;
    }
};


export default cardReducer;
