import {ADD_DISHES} from "../actions/actionTypes";

const initialState = {
    dishes: {}
};

const dishesReducer = (state = initialState, action) => {
    switch (action.type) {
        case ADD_DISHES:
            return {
                ...state
            };
        default: return state
    }
};

export default dishesReducer;
