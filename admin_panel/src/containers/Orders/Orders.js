import React, {Component, Fragment} from 'react';
import Navigation from "../../components/Navigaiton/Navigation";
import {connect} from "react-redux";
import {deleteOrder, fetchGetDishes, fetchGetOrders} from "../../store/actions/cardBuilder";

class Orders extends Component {

    componentDidMount() {
        this.props.fetchDishes();
        this.props.fetchGetOrders();
    }

    render() {
        if (!this.props.dishes || !this.props.orders) {
            return <div>Loading...</div>
        }
        const orders = Object.keys(this.props.orders).map(orderId => {
            let orderPrice = 0;
            let order = this.props.orders[orderId].order;
            console.log(order);
            return (
               <div key={orderId}>
                   <p><strong>Order ID: {orderId}</strong></p>
                   {
                       Object.keys(order).map(dishId => {
                           const dish = this.props.dishes[dishId];
                           const orderQty = order[dishId];
                           const orderItemPrice = dish.price * orderQty;
                           orderPrice += orderItemPrice;
                           return (
                               <p key={dishId}><strong>{dish.title}</strong>: x{orderQty} = {orderItemPrice}</p>
                           )
                       })
                   }
                   <p><strong>Delivery price: </strong>150 KGS</p>
                   <p><strong>Total price: </strong>{orderPrice + 150}</p>
                   <button className="complete-btn" onClick={() => this.props.deleteOrder(orderId)}>Complete order</button>
               </div>
           )
        });
        return (
            <Fragment>
                <Navigation/>
                <div className="order-block">
                    {orders}
                </div>
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    dishes: state.card.dishes,
    orders: state.card.orders,
});

const mapDispatchToProps = dispatch => ({
    fetchDishes: () => dispatch(fetchGetDishes()),
    fetchGetOrders: () => dispatch(fetchGetOrders()),
    deleteOrder: (orderId) => dispatch(deleteOrder(orderId)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Orders);

