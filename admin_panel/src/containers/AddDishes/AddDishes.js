import React, {Component, Fragment} from 'react';
import DishesForm from "../../components/DishesForm/DishesForm";
import Navigation from "../../components/Navigaiton/Navigation";
import {fetchPostDish} from "../../store/actions/dishesBuilder";
import {connect} from "react-redux";

class AddDishes extends Component {
    addNewDish = (dish) => {
      this.props.sendDishes(dish);
      this.props.history.push('/');
    };

    render() {
        return (
            <Fragment>
                <Navigation/>
                <h4 className="form-title">Add Dish</h4>
                <DishesForm
                    onSubmit={this.addNewDish}
                />
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    dishes: state.dishes.dishes,
});

const mapDispatchToProps = dispatch => ({
    sendDishes: (dish) => dispatch(fetchPostDish(dish)),
});

export default connect(mapStateToProps, mapDispatchToProps)(AddDishes);

