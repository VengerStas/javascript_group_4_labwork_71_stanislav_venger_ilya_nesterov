import React, {Component, Fragment} from 'react';
import {NavLink} from "react-router-dom";
import Navigation from "../../components/Navigaiton/Navigation";

import {connect} from "react-redux";
import {deleteDish, fetchGetDishes} from "../../store/actions/cardBuilder";

import "./DishesList.css";

class DishesList extends Component {

    componentDidMount() {
        this.props.loadDishes();
    };

    goToEditPage = (id) => {
        this.props.history.push(`/${id}/edit`);
    };

    render() {
        let dish = Object.keys(this.props.dishes).map ((id) => {
            return (
                <div key={id} className="dish">
                    <img className="dish-image" src={this.props.dishes[id].image} alt={this.props.dishes[id].title}/>
                    <p className="dish-name">"{this.props.dishes[id].title}"</p>
                    <p className="dish-price">{this.props.dishes[id].price} KGS</p>
                    <div className="controls">
                        <button className="btn edit" onClick={() => this.goToEditPage(id)}>Edit</button>
                        <button className="btn delete" onClick={() => this.props.deleteDish(id)}>Delete</button>
                    </div>
                </div>
            )
        });

        return (
            <Fragment>
                <Navigation/>
                <div className="add-block">
                    <h4 className="dishes-title">Dishes</h4>
                    <NavLink to="/add-dish" className="add-dish">Add new Dish</NavLink>
                </div>
                <div className="dishes-block">
                    {dish}
                </div>
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    dishes: state.card.dishes,
});

const mapDispatchToProps = dispatch => ({
    loadDishes: () => dispatch(fetchGetDishes()),
    deleteDish: (dishId) => dispatch(deleteDish(dishId)),
});

export default connect(mapStateToProps, mapDispatchToProps)(DishesList);

