import React, {Component, Fragment} from 'react';
import Navigation from "../../components/Navigaiton/Navigation";
import DishesForm from "../../components/DishesForm/DishesForm";
import {editDish, saveEditDish} from "../../store/actions/cardBuilder";
import {connect} from "react-redux";

class EditDishes extends Component {

    componentDidMount () {
        this.props.editDish(this.props.match.params.id);
    }

    editCurrentDish = (dishEdit) => {
        this.props.saveEditDish(this.props.match.params.id, dishEdit);
        this.props.history.push('/');
    };

    render() {
        let form = <DishesForm
            onSubmit={this.editCurrentDish}
            dishes={this.props.dish}
        />;

        if (!this.props.dish) {
            form = <div>Loading Data...</div>
        }

        return (
            <Fragment>
                <Navigation/>
                <div className="edit-block">
                    <h4 className="edit-title">Edit Dish</h4>
                    {form}
                </div>
            </Fragment>
        );
    }
}

const mapStateToProps = state => {
    return {
        dish: state.card.dish,
    };
};

const mapDispatchToProps = dispatch => {
    return {
        editDish: (dishId) => dispatch(editDish(dishId)),
        saveEditDish: (dishId, dish) => dispatch(saveEditDish(dishId, dish)),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(EditDishes);

