import React, {Component} from 'react';
import {NavLink} from "react-router-dom";

import "./Navigation.css";

class Navigation extends Component {
    render() {
        return (
            <header className="header">
                <h1 className="company-name">USSR Buffet</h1>
                <nav className="menu">
                    <ul className="nav-menu">
                        <li className="nav-item">
                            <NavLink to="/" className="nav-link">Dishes</NavLink>
                        </li>
                        <li className="nav-item">
                            <NavLink to="/orders" className="nav-link">Orders</NavLink>
                        </li>
                    </ul>
                </nav>
            </header>
        );
    }
}

export default Navigation;
