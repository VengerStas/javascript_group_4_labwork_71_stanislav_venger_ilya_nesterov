import React, {Component} from 'react';

import './DishesForm.css';

class DishesForm extends Component {

            // state = {
            //     title: '',
            //     price: '',
            //     image: '',
            // };
            //
            // componentDidMount() {
            //     this.setState({...this.props.dishes})
            // }

    constructor(props) {
        super (props);
        if (this.props.dishes) {
            this.state = {...props.dishes}
        } else {
            this.state = {
                name: '',
                price: '',
                image: '',
            };
        }
    }

    getInputValue = event => {
        const {name, value} = event.target;
        console.log(value);
        this.setState({[name]: value});
    };

    saveDishHandler = event => {
        event.preventDefault();
        this.props.onSubmit({...this.state});
    };

    render() {
        return (
            <div className="form-block">
                <form onSubmit={this.saveDishHandler}>
                    <input type="text" name="title" value={this.state.title} onChange={(e) => this.getInputValue(e)} placeholder="Dish name" className="input"/>
                    <input type="number" name="price" value={this.state.price} onChange={(e) => this.getInputValue(e)} placeholder="Dish price" className="input"/>
                    <input type="url" name="image" value={this.state.image} onChange={(e) => this.getInputValue(e)} placeholder="Url for Dish image" className="input"/>
                    <button className="save" type="submit">Save</button>
                </form>
            </div>
        );
    }
}

export default DishesForm;
