import axios from 'axios';

const instance = axios.create({
  baseURL: 'https://ussr-buffet.firebaseio.com/'
});

export default instance;
