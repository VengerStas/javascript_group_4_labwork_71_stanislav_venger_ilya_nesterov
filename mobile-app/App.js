import React from 'react';

import {applyMiddleware, createStore} from 'redux';
import {Provider} from 'react-redux';
import thunkMiddleware from 'redux-thunk';

import Menu from "./components/Menu/Menu";
import reducer from "./store/reducer";

const store = createStore(reducer, applyMiddleware(thunkMiddleware));

const app = () =>  (
    <Provider store={store}>
        <Menu/>
    </Provider>
);

export default app;

