import React, {Component} from 'react';
import {FlatList, StyleSheet, Text, View, TouchableOpacity, Image, ImageBackground, TextInput, Modal} from "react-native";
import {connect} from 'react-redux';
import {addDish, deleteDish, menuDishes, toggleModal, pushDeliveryDishes} from "../../store/actions";
import container from '../../assets/menu.jpg';
import trash from '../../assets/trash.jpeg';
import logo from '../../assets/logo.jpeg';

class Menu extends Component {
    state = {
        onRefresh: false,
        name: '',
        numberPhone: '',
        address: '',
    };

    sentOrderHandler = () => {
        const manOrder = {
            name: this.state.name,
            numberPhone: this.state.numberPhone,
            address: this.state.address
        };
        this.props.pushDeliveryDishes(manOrder)
    };

    componentDidMount() {
        this.props.menuDishes();
    }

    onRefresh = () => {
        this.setState({onRefresh: true});
        this.props.menuDishes();
    };

    render() {
        let order = Object.keys(this.props.dishes).map(item => {
            if (this.props.dishesQuantity[item] < 1) return null;
            return (
                <View key={item} style={styles.dishesOrder}>
                        <Text style={styles.textOrder}>{this.props.dishes[item].title}</Text>
                        <Text style={styles.textOrder}>x {this.props.dishesQuantity[item]}</Text>
                        <Text style={styles.textOrder}>{this.props.dishes[item].price * this.props.dishesQuantity[item]} KGS</Text>
                        <TouchableOpacity onPress={() => this.props.deleteDish(item, this.props.dishes[item].price)}>
                            <Image source={trash} style={{width: 25, height: 25, borderRadius: 5}}/>
                        </TouchableOpacity>
                </View>
        )});
        let dishes = Object.keys(this.props.dishes).map(item => {
            return {
                id: item,
                ...this.props.dishes[item]
            }
        });
        return (
            <ImageBackground source={container} style={{width: '100%', height: '100%'}}>
            <View style={styles.container}>
                <View style={styles.menu}>
                    <View>
                        <Text style={styles.text}>Menu</Text>
                    </View>
                    <View>
                        <Image source={logo} style={{width: 40, height: 40, borderRadius: 15}}/>
                    </View>
                </View>
                <FlatList
                    data={dishes}
                    onRefresh={() => this.onRefresh()}
                    refreshing={this.props.loading}
                    keyExtractor={(item) => item.id}
                    renderItem={({item}) => (
                        <TouchableOpacity style={styles.dishes} onPress={() => this.props.addDish({id: item.id, price: item.price})}>
                                <Image
                                    source={{uri: item.image}}
                                    style={{width: 60, height: 60, margin: 10}}
                                />
                                <Text style={styles.dish}>{item.title}</Text>
                                <Text style={styles.dishPrice}>{item.price} KGS</Text>
                        </TouchableOpacity>
                    )}
                />
                <View style={styles.menuSubmit}>
                    <Text style={styles.submit}>You price: {this.props.totalPrice} KGS</Text>
                    <TouchableOpacity style={styles.order} onPress={this.props.toggleModal}>
                        <Text style={{fontSize: 20}}>Submit</Text>
                    </TouchableOpacity>
                </View>
                <Modal
                    animationType="fade"
                    transparent={false}
                    visible={this.props.modalVisible}
                    onRequestClose={this.props.toggleModal}>
                    <View style={styles.modal}>
                        <View>
                            {order}
                        </View>
                        <View style={{flexDirection: 'column', justifyContent: 'space-around',}}>
                            <View style={{flexDirection: 'row', justifyContent: 'space-between', paddingHorizontal: 15}}>
                                <Text style={styles.text}>Delivery:</Text>
                                <Text style={styles.text}> 150 KGS</Text>
                            </View>
                            <View  style={{flexDirection: 'row', justifyContent: 'space-between', paddingHorizontal: 15}}>
                                <Text style={styles.text}>Total price:</Text>
                                <Text style={styles.text}> {this.props.totalPrice} KGS</Text>
                            </View>
                        </View>
                        <View>
                            <TextInput
                                style={styles.input}
                                value={this.state.name}
                                onChangeText={(text) => this.setState({name: text})}
                                placeholder='Your name'
                            />
                            <TextInput
                                style={styles.input}
                                value={this.state.numberPhone}
                                onChangeText={(number) => this.setState({numberPhone: number})}
                                placeholder='Your phone'
                            />
                            <TextInput
                                style={styles.input}
                                value={this.state.address}
                                onChangeText={(address) => this.setState({address: address})}
                                placeholder='Your address'
                            />
                        </View>
                        <View>
                        <TouchableOpacity style={styles.treatment} onPress={this.sentOrderHandler}>
                            <Text style={styles.textTreatment}>Order</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.treatment} onPress={this.props.toggleModal}>
                            <Text style={styles.textTreatment}>Close</Text>
                        </TouchableOpacity>
                        </View>
                    </View>
                </Modal>
            </View>
            </ImageBackground>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'stretch',
        textAlign: 'center',
        paddingTop: 25,
        fontSize: 20
    },
    menu: {
        paddingVertical: 20,
        backgroundColor: 'rgba(126, 190, 180, 0.8)',
        borderRadius: 5,
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingHorizontal: 20
    },
    text: {
        fontSize: 20,
        paddingVertical: 8
    },
    dishes: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        backgroundColor: 'rgba(230, 226, 214, 0.9)',
        margin: 5,
        borderRadius: 5,
    },
    dish: {
        padding: 25,
        color: '#000',
        fontSize: 20,
    },
    dishPrice: {
        flex: 4,
        padding: 25,
        fontSize: 20,
        textAlign: 'right',
    },
    menuSubmit: {
        flexDirection: 'row',
        paddingVertical: 20,
        borderRadius: 5,
        backgroundColor: 'rgba(126, 190, 180, 0.8)'
    },
    order: {
        backgroundColor: 'rgba(230, 226, 214, 0.9)',
        borderRadius: 5,
        paddingVertical: 10,
        paddingHorizontal: 25,
        marginHorizontal: 10
    },
    submit: {
        fontSize: 20,
        flex: 8,
        paddingHorizontal: 10,
        paddingVertical: 10
    },
    treatment: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        backgroundColor: '#0009c6',
        margin: 5,
        paddingVertical: 10,
        borderRadius: 5,
    },
    dishesOrder: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingHorizontal: 15,
        paddingVertical: 5,
    },
    textOrder: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        fontSize: 20
    },
    textTreatment: {
        color: '#fff',
        fontSize: 20,
        textAlign: 'center'
    },
    modal: {
        paddingTop: '10%',
        backgroundColor: 'rgba(230, 226, 214, 0.9)',
        width: '100%',
        height: '100%',
        flexDirection: 'column',
        justifyContent: 'space-between'
    },
    input: {
        width: 330,
        borderColor: 'black',
        borderWidth: 1,
        paddingHorizontal: 10,
        paddingVertical: 5,
        marginHorizontal: 15,
        marginVertical: 10,
        borderRadius: 5,
    }
});

const mapStateToProps = state => {
    return {
        dishes: state.dishes,
        dishesQuantity: state.dishesQuantity,
        loading: state.loading,
        modalVisible: state.modalVisible,
        totalPrice: state.totalPrice
    };
};

const mapDispatchToProps = dispatch => {
    return {
        menuDishes: () => dispatch(menuDishes()),
        toggleModal: () => dispatch(toggleModal()),
        addDish: order => dispatch(addDish(order)),
        deleteDish: (name, price) => dispatch(deleteDish(name, price)),
        pushDeliveryDishes: manOrder => dispatch(pushDeliveryDishes(manOrder))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Menu);
