import axios from '../axiosBase';

export const TOGGLE_MODAL = 'TOGGLE_MODAL';
export const ADD_DISH = 'ADD_DISH';
export const DELETE_DISHES = 'DELETE_DISHES';

export const DISHES_REQUEST = 'DISHES_REQUEST';
export const DISHES_SUCCESS = 'DISHES_SUCCESS';
export const DISHES_ERROR = 'DISHES_ERROR';

export const toggleModal = () => {
    return {type: TOGGLE_MODAL};
};

export const addDish = order => {
    return {type: ADD_DISH, order};
};

export const deleteDish = (name, price) => {
    return {type: DELETE_DISHES, name, price}
};

export const dishesRequest = () => {
    return {type: DISHES_REQUEST};
};

export const dishesSuccess = dishes => {
    return {type: DISHES_SUCCESS, dishes};
};

export const dishesError = error => {
    return {type: DISHES_ERROR, error};
};

export const menuDishes = () => {
    return (dispatch) => {
        dispatch(dishesRequest());
        axios.get('/dishes.json').then(response => {
            dispatch(dishesSuccess(response.data));
        }, error => {
            dispatch(dishesError(error));
        });
    }
};

export const pushDeliveryDishes = manOrder => {
    return (dispatch, getState) => {
            const order = {
                manOrder: manOrder,
                order: getState().dishesQuantity
            };
        dispatch(dishesRequest());
        axios.post('/orders.json', order).then(() => {
            dispatch(toggleModal());
            dispatch(menuDishes());
        }, error => {
            dispatch(dishesError(error))
        })
    }
};