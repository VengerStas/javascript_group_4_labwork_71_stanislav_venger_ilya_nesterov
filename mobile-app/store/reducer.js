import {
    ADD_DISH,
    DELETE_DISHES,
    DISHES_ERROR,
    DISHES_REQUEST,
    DISHES_SUCCESS,
    TOGGLE_MODAL
} from "./actions";

const initialState = {
    dishes: [],
    dishesQuantity: [],
    modalVisible: false,
    loading: false,
    totalPrice: 150,
};

const reducer = (state = initialState, action) => {
    switch(action.type) {
        case TOGGLE_MODAL:
            return {
                ...state,
                modalVisible: !state.modalVisible,
                totalPrice: state.totalPrice,
            };
        case ADD_DISH:
            return {
                ...state,
                dishesQuantity: {...state.dishesQuantity, [action.order.id] : state.dishesQuantity[action.order.id] + 1},
                totalPrice: state.totalPrice + parseInt(action.order.price),
            };
        case DELETE_DISHES:
            if (state.dishes[action.name] === 0) return state;
            return {
                ...state,
                dishesQuantity: {...state.dishesQuantity, [action.name] : state.dishesQuantity[action.name] - 1},
                totalPrice: state.totalPrice - action.price,
            };
        case DISHES_REQUEST:
            return {
                ...state,
                totalPrice: 150,
            };
        case DISHES_SUCCESS:
            let newDishes = {};
            Object.keys(action.dishes).forEach(dish => {
                newDishes[dish] = 0;
            });
            return {
                ...state,
                dishes: action.dishes,
                dishesQuantity: newDishes
            };
        case DISHES_ERROR:
            return {
                ...state,
            };
        default:
        return state;
    }

};

export default reducer;